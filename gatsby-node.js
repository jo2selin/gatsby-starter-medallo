const path = require(`path`);
// Log out information after a build is done
exports.onPostBuild = ({ reporter }) => {
  reporter.info(`Your Gatsby site has been built!`);
};
// Create blog pages dynamically
exports.createPages = async ({ graphql, actions }) => {
  const { createPage } = actions;
  const pageTemplate = path.resolve(`src/templates/page.js`);
  const result = await graphql(`
    query {
      allDatoCmsPage {
        edges {
          node {
            slug
            locale
            _allSlugLocales {
              locale
              value
            }
          }
        }
      }
      datoCmsSite {
        domain
        locales
      }
    }
  `);

  result.data.allDatoCmsPage.edges.forEach((edge) => {
    if (edge.node.slug === null) return false;
    const allWebsiteLocales = result.data.datoCmsSite.locales;
    const mainLanguage = result.data.datoCmsSite.locales[0];
    const slug = edge.node.slug === "homepage" ? "" : edge.node.slug;
    const path =
      edge.node.locale === mainLanguage
        ? slug + "/"
        : `${edge.node.locale}/${slug}`;

    createPage({
      path: path,
      component: pageTemplate,
      context: {
        locale: edge.node.locale,
        slug: edge.node.slug,
        _allSlugLocales: edge.node._allSlugLocales,
        allWebsiteLocales: allWebsiteLocales,
      },
    });
  });
};
