import React from "react";
import { ThemeProvider } from "./src/context/ThemeContext";

// exports.onInitialClientRender = () => {
//   console.log("onInitialClientRender========");
//   const script = document.createElement("script");

//   script.src = "https://cdn.jsdelivr.net/npm/@editorjs/editorjs@latest";
//   document.body.appendChild(script);

//   script.addEventListener("load", () => {
//     console.log("script loaded");
//   });
// };

export const wrapRootElement = ({ element }) => (
  <ThemeProvider>{element}</ThemeProvider>
);
