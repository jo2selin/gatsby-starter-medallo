import PropTypes from "prop-types";
import { css } from "@emotion/react";
import DatoLink from "./datoLink";
import { Link } from "gatsby";
import { mq } from "../utils";
import LangSwitcher from "../components/langSwitcher";

interface itemsMenu {
  id: string;
  title: string;
  description: string;
  externalLink: string | null;
  internalLink: {
    slug: string;
    locale: string;
    _allSlugLocales: { locale: string; value: string }[];
  } | null;
  _allTitleLocales: { locale: string; value: string }[];
  _allDescriptionLocales: { locale: string; value: string }[];
}

interface interfaceLink {
  title: string;
  externalLink: string | null;
  internalLink: {
    slug: string;
    locale: string;
    _allSlugLocales: { locale: string; value: string }[];
  } | null;
  description: string;
}
interface menuProp {
  data: {
    edges: {
      node: {
        locale: string;
        items: {
          __typename: string;
          id: string;
          title: string;
          link: interfaceLink;
          theme: {
            primary: { hex: { hex: string } };
            background: { hex: { hex: string } };
            secondary: { hex: { hex: string } };
          };
          externalLink: string | null;
          internalLink: {
            slug: string;
            locale: string;
            _allSlugLocales: { locale: string; value: string }[];
          } | null;
          items: itemsMenu[];
        }[];
      };
    }[];
    items: any[];
  };
  _allSlugLocales: { locale: string; value: string }[];
  allWebsiteLocales: [];
  locale: string;
}

interface submenuProp {
  submenu: {
    title: string;
    items: itemsMenu[];
  };
  locale: string;
}

// There is only one menu for all locales. Find translation of each item
const matchLocaleText = (
  allTrads: { locale: string; value: string }[],
  locale: String
) => {
  const trad = allTrads.filter((trad) => trad.locale === locale);
  return trad[0]?.value;
};

// const matchLocaleLink = (
//   allTrads: { locale: string; value: string }[],
//   locale: String
// ) => {
//   const trad = allTrads.filter((trad) => trad.locale === locale);
//   return trad[0]?.value;
// };

const Submenu = ({ submenu, locale }: submenuProp) => {
  return (
    <div className="navbar-item has-dropdown is-hoverable">
      <a className="navbar-link">{submenu.title}</a>

      <div className="navbar-dropdown">
        {submenu.items.map((item) => {
          console.log(item);

          return (
            <div key={item.id} css={subMenuItemCss}>
              <DatoLink link={item} locale={locale} className={"navbar-item"}>
                <p className={"title is-6"}>
                  {matchLocaleText(item._allTitleLocales, locale) || item.title}
                </p>
                {item.description && (
                  <div className={"subtitle is-6"}>
                    {matchLocaleText(item._allDescriptionLocales, locale) ||
                      item.description}
                  </div>
                )}
              </DatoLink>
            </div>
          );
        })}
        {/* <hr className="navbar-divider" /> */}
      </div>
    </div>
  );
};

const Menu = ({
  data,
  locale,
  _allSlugLocales,
  allWebsiteLocales,
}: menuProp) => {
  return (
    <nav className="navbar" role="navigation" aria-label="main navigation">
      <div className="container">
        <div className="navbar-brand">
          <Link to={"/"} className="navbar-item">
            <img
              src="https://bulma.io/images/bulma-logo.png"
              width="112"
              height="28"
            />
          </Link>

          <a
            role="button"
            className="navbar-burger"
            aria-label="menu"
            aria-expanded="false"
            data-target="navbarBasicExample"
          >
            <span aria-hidden="true"></span>
            <span aria-hidden="true"></span>
            <span aria-hidden="true"></span>
          </a>
        </div>

        <div id="navbarBasicExample" className="navbar-menu">
          <div className="navbar-start">
            {data.items.map((item) => {
              switch (item.__typename) {
                case "DatoCmsLink":
                  return (
                    <DatoLink
                      key={item.id}
                      link={item}
                      className={"navbar-item"}
                      locale={locale}
                    >
                      {matchLocaleText(item._allTitleLocales, locale) ||
                        item.title}
                    </DatoLink>
                  );
                  break;
                case "DatoCmsSubmenu":
                  return (
                    <Submenu key={item.id} submenu={item} locale={locale} />
                  );
                  break;
                case "DatoCmsButton":
                  return (
                    <div key={item.id} className="navbar-item">
                      <DatoLink
                        link={item.link}
                        className={"button"}
                        theme={item.theme}
                        locale={locale}
                      >
                        <strong>
                          {matchLocaleText(item._allTitleLocales, locale) ||
                            item.title}
                        </strong>
                      </DatoLink>
                    </div>
                  );
                  break;

                default:
                  break;
              }
              return null;
            })}
          </div>
          <div className="navbar-end">
            <LangSwitcher
              _allSlugLocales={_allSlugLocales}
              allWebsiteLocales={allWebsiteLocales}
              pageLocale={locale}
            />
          </div>
        </div>
      </div>
    </nav>
  );
};

const subMenuItemCss = css`
  ${mq[1]} {
    width: 350px;
  }

  a.navbar-item {
    display: flex;
    flex-direction: column;
    align-items: flex-start;
    white-space: initial;
  }
`;

Menu.propTypes = {
  data: PropTypes.shape({
    edges: PropTypes.arrayOf(
      PropTypes.shape({
        node: PropTypes.shape({
          items: PropTypes.array.isRequired,
        }),
      })
    ),
  }),
};

export default Menu;
