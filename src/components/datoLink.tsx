import PropTypes from "prop-types";
import { css } from "@emotion/react";
import { Link } from "gatsby";
import { linkResolver } from "../utils";

interface internalLink {
  slug: string;
  locale: string;
  _allSlugLocales: { locale: string; value: string }[];
}
interface linkProp {
  link: {
    externalLink: string | null;
    internalLink: internalLink | null;
  };
  locale: string;
  className?: string;
  theme?: {
    primary: { hex: { hex: string } };
    background: { hex: { hex: string } };
    secondary: { hex: { hex: string } };
  };
  children: any;
}

const setInternalLink = (internalLink: internalLink, locale: string) => {
  // if the acutal locale === the locale of the main record, no need to search for _allSlugLocales
  if (internalLink.locale === locale)
    return linkResolver(internalLink.slug, locale);
  // find the localized slug in the _allSlugLocales
  const matchLocale = internalLink._allSlugLocales.filter(
    (slug) => slug.locale === locale
  );
  if (matchLocale[0]) {
    return linkResolver(matchLocale[0].value, locale);
  } else {
    console.warn(
      `a link to the page: ${internalLink.slug} does not have any content for the locale: ${locale}. Using ${internalLink.locale}'s slug instead `
    );
    return linkResolver(internalLink.slug, internalLink.locale);
  }
};

const DatoLink = ({ link, locale, className, theme, children }: linkProp) => {
  const buttonThemeCss = css`
    &.button {
      background: ${theme?.primary.hex.hex};
      color: ${theme?.background.hex.hex};
      border: none;
      &:hover {
        background: ${theme?.secondary.hex.hex};
        color: ${theme?.background.hex.hex};
      }
    }
  `;

  if (link.internalLink) {
    if (link.internalLink.slug === null) {
      console.error(
        `link destination is null (${link.internalLink.slug}). locale: ${link.internalLink.locale}`,
        link.internalLink
      );
    }

    return (
      <Link
        to={
          "/" +
          setInternalLink(link.internalLink, locale || link.internalLink.locale)
        }
        className={className}
        css={theme && buttonThemeCss}
      >
        {children}
      </Link>
    );
  } else if (link.externalLink) {
    return (
      <a
        href={link.externalLink}
        className={className}
        target="_blank"
        rel="noopener"
      >
        {children}
      </a>
    );
  } else console.error("error link: ", link);
  return null;
};

DatoLink.propTypes = {
  link: PropTypes.object.isRequired,
  className: PropTypes.string,
  theme: PropTypes.object,
  children: PropTypes.any.isRequired,
};

export default DatoLink;
