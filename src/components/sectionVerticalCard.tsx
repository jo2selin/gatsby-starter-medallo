import PropTypes from "prop-types";
import { css } from "@emotion/react";
import { useTheme } from "@emotion/react";

import clsx from "clsx";
// import MainCard from "./mainCard"
// import MainSidedBlock from "./mainSidedBlock"

interface cardsProp {
  data: {
    layout: string;
    cards: {
      id: string;
      title: string;
      paragraphNode: { childMarkdownRemark: { html: string } };
    }[];
  };
}

const SectionVerticalCard = ({ data }: cardsProp) => {
  const theme = useTheme();

  return (
    <div className="columns is-multiline  ">
      {data.cards.map((maincard) => (
        <div key={maincard.id} className="column is-one-third">
          <div className={clsx("card", data.layout)}>
            <div className="card-content">
              <div className="content">
                <h2
                  className={clsx("title is-4")}
                  css={theme.isDark && cssIsDark}
                >
                  {maincard.title}
                </h2>
              </div>
            </div>
          </div>
        </div>
        // <MainCard key={i} mainCard={maincard} />
      ))}
    </div>
  );
};

const cssIsDark = css`
  &.title,
  p {
    color: darkgray;
  }
`;

SectionVerticalCard.propTypes = {
  data: PropTypes.object.isRequired,
};

export default SectionVerticalCard;
