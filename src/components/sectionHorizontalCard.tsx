import PropTypes from "prop-types";
import { css } from "@emotion/react";
import { useTheme } from "@emotion/react";

import clsx from "clsx";
// import MainCard from "./mainCard"
// import MainSidedBlock from "./mainSidedBlock"

interface cardsProp {
  data: {
    layout: String;
    cards: {
      id: string;
      title: String;
      paragraphNode: { childMarkdownRemark: { html: string } };
    }[];
  };
}

const SectionHorizontalCard = ({ data }: cardsProp) => {
  const theme = useTheme();

  return (
    <div className="columns ">
      {data.cards.map((maincard) => (
        <div key={maincard.id} className="column">
          <div
            className={clsx("card", data.layout)}
            css={data.layout === "no-background" && CssNoBackground}
          >
            <div className="card-content">
              <div className="content">
                <h2
                  className={clsx("title is-4")}
                  css={theme.isDark && cssIsDark}
                >
                  {maincard.title}
                </h2>
                <div
                  css={theme.isDark && cssIsDark}
                  dangerouslySetInnerHTML={{
                    __html: maincard.paragraphNode.childMarkdownRemark.html,
                  }}
                />
              </div>
            </div>
          </div>
        </div>
        // <MainCard key={i} mainCard={maincard} />
      ))}
    </div>
  );
};

const CssNoBackground = css`
  background: none;
  &.card {
    background: none;
  }
`;

const cssIsDark = css`
  &.title,
  p {
    color: darkgray;
  }
`;

SectionHorizontalCard.propTypes = {
  data: PropTypes.object.isRequired,
};

export default SectionHorizontalCard;
