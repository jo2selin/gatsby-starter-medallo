import { GatsbyImage } from "gatsby-plugin-image";
import { css } from "@emotion/react";
import clsx from "clsx";
import { useTheme } from "@emotion/react";

interface SidedProps {
  data: {
    title: String;
    side: Boolean;
    blocks: {
      title: String;
      image: { gatsbyImageData: any };
      paragraphNode: { childMarkdownRemark: { html: string } };
      buttons: { style: any; title: string }[];
      theme: {
        background: { hex: { hex: string } };
        primary: { hex: { hex: string } };
        isDark: boolean;
      };
    }[];
  };
}

const SidedContent = ({ data }: SidedProps) => {
  const theme = useTheme();

  return (
    <div
      className={clsx("columns has-text-left")}
      css={data.side && cssIsReversed}
    >
      <div className="column is-three-fifths">
        <div className="content">
          <h2 className={clsx("title is-4", theme.isDark && "has-text-white")}>
            {data.blocks[0].title}
          </h2>
          {data.blocks[0].image && (
            <figure className="image">
              <GatsbyImage
                image={data.blocks[0].image.gatsbyImageData}
                alt={"alt"}
                className="w-full align-middle rounded-t-lg"
              />
            </figure>
          )}
          <div
            dangerouslySetInnerHTML={{
              __html: data.blocks[0].paragraphNode.childMarkdownRemark.html,
            }}
          />
        </div>
      </div>
      <div className="column">
        <div
          className="card"
          css={
            data.blocks[1].theme.background &&
            css`
              background: ${data.blocks[1].theme.background.hex.hex};
            `
          }
        >
          {data.blocks[1].image && (
            <div className="card-image">
              <figure className="image">
                <GatsbyImage
                  image={data.blocks[1].image.gatsbyImageData}
                  alt={"alt"}
                  className="w-full align-middle rounded-t-lg"
                />
              </figure>
            </div>
          )}
          <div className="card-content">
            <div className="content">
              <h2
                className={clsx(
                  "title is-4",
                  data.blocks[1].theme.isDark && "has-text-white"
                )}
              >
                {data.blocks[1].title}
              </h2>

              <div
                className={clsx(
                  data.blocks[1].theme.isDark && "has-text-white"
                )}
                dangerouslySetInnerHTML={{
                  __html: data.blocks[1].paragraphNode.childMarkdownRemark.html,
                }}
              />
            </div>
            {data.blocks[1].buttons && (
              <div className="buttons">
                {data.blocks[1].buttons.map((button) => (
                  <div
                    className={clsx(
                      "button",
                      button.style && "is-" + button.style
                    )}
                  >
                    {button.title}
                  </div>
                ))}
              </div>
            )}
          </div>
        </div>
      </div>
    </div>
  );
};

const cssIsReversed = css`
  flex-direction: row-reverse;
`;

export default SidedContent;
