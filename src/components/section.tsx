import PropTypes from "prop-types";
import { css } from "@emotion/react";
import { useTheme } from "@emotion/react";

import clsx from "clsx";
interface sectionProp {
  data: {
    // background: { hex: { hex: any } };
    title: string;
    paragraphNode: { childMarkdownRemark: { html: string } };
    size: String;
  };
  children: object;
}

declare module "@emotion/react" {
  export interface Theme {
    background: {
      hex: { hex: string };
    };
    primary: {
      hex: { hex: string };
    };
    isDark: boolean;
  }
}

const Section = ({ data, children }: sectionProp) => {
  const theme = useTheme();

  return (
    <section
      className={clsx(
        "section has-text-centered",
        theme.isDark && "has-text-white"
      )}
      css={
        theme.background &&
        css`
          background: ${theme.background.hex.hex};
        `
      }
    >
      <div className={clsx("container", data.size)}>
        <h1 className={clsx("title", theme.isDark && "has-text-white")}>
          {data.title}
        </h1>
        <div
          className={clsx("subtitle", theme.isDark && "has-text-white")}
          dangerouslySetInnerHTML={{
            __html: data.paragraphNode.childMarkdownRemark.html,
          }}
        />
        {children}
      </div>
    </section>
  );
};

Section.propTypes = {
  data: PropTypes.object.isRequired,
  children: PropTypes.any.isRequired,
};

export default Section;
