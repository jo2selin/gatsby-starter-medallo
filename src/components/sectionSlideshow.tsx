import PropTypes from "prop-types";
import { css } from "@emotion/react";
import { useTheme } from "@emotion/react";
import Slider from "react-slick";

import clsx from "clsx";
// import MainCard from "./mainCard"
// import MainSidedBlock from "./mainSidedBlock"

interface cardsProp {
  data: {
    layout: String;
    cards: {
      id: string;
      title: String;
      paragraphNode: { childMarkdownRemark: { html: string } };
    }[];
  };
}

const SectionSlideshow = ({ data }: cardsProp) => {
  const theme = useTheme();

  const sliderSettings = {
    dots: true,
    infinite: true,
    speed: 500,
    slidesToShow: 1,
    slidesToScroll: 1,
    autoplay: true,
    autoplaySpeed: 2000,
    pauseOnHover: true,
  };

  return (
    <Slider {...sliderSettings}>
      {data.cards.map((card) => (
        <div
          key={card.id}
          className={clsx("", data.layout)}
          css={data.layout === "slideshow" && CssSlideshow}
        >
          <div className="card-content">
            <div className="content">
              <h2
                className={clsx("title is-4")}
                css={theme.isDark && cssIsDark}
              >
                {card.title}
              </h2>
              <div
                css={theme.isDark && cssIsDark}
                dangerouslySetInnerHTML={{
                  __html: card.paragraphNode.childMarkdownRemark.html,
                }}
              />
            </div>
          </div>
        </div>
      ))}
    </Slider>
  );
};

const CssSlideshow = css`
  /* background: none; */
  /* padding: 0 10%; */
  max-width: 800px;
`;

const cssIsDark = css`
  &.title,
  p {
    color: darkgray;
  }
`;

SectionSlideshow.propTypes = {
  data: PropTypes.object.isRequired,
};

export default SectionSlideshow;
