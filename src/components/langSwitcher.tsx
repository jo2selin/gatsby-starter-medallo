import PropTypes from "prop-types";
import DatoLink from "./datoLink";

interface interfaceLangSwitcher {
  _allSlugLocales: { locale: string; value: string }[];
  pageLocale: string;
  allWebsiteLocales: [];
}

interface interfaceDisplayLocale {
  displayLocale: {
    locale: string;
    value: string;
  };
}

const findTraducedPage = (
  locale: string,
  _allSlugLocales: { locale: string; value: string }[]
) => {
  let foundTranslation = _allSlugLocales.filter(
    (slugLocale) => slugLocale.locale === locale
  );
  if (foundTranslation) return foundTranslation[0];
  else return false;
};

const DisplayLocale = ({ displayLocale }: interfaceDisplayLocale) => {
  return (
    <div key={displayLocale.locale} className="field has-addons  navbar-item">
      <p className="control">
        <DatoLink
          link={{
            internalLink: {
              slug: displayLocale.value,
              locale: displayLocale.locale,
            },
            externalLink: null,
          }}
        >
          <button className="button is-primary">
            <span>{displayLocale.locale}</span>
          </button>
        </DatoLink>
      </p>
    </div>
  );
};

const LangSwitcher = ({
  _allSlugLocales,
  pageLocale,
  allWebsiteLocales,
}: interfaceLangSwitcher) => {
  return (
    <nav className="navbar" role="navigation" aria-label="main navigation">
      {allWebsiteLocales.map((websiteLocale) => {
        //map around all locale available on the website

        // find if this page has a traduction
        const traducedPages = findTraducedPage(websiteLocale, _allSlugLocales);

        // if this is a different locale than the actual one
        if (websiteLocale !== pageLocale)
          if (traducedPages) {
            // if there is a translation available
            return (
              <DisplayLocale
                key={websiteLocale}
                displayLocale={traducedPages}
              />
            );
          } else {
            // no translation of this page, show link to homepage
            return (
              <DisplayLocale
                key={websiteLocale}
                displayLocale={{ locale: websiteLocale, value: "" }}
              />
            );
          }
      })}
    </nav>
  );
};

LangSwitcher.propTypes = {
  data: PropTypes.shape({
    edges: PropTypes.arrayOf(
      PropTypes.shape({
        node: PropTypes.shape({
          items: PropTypes.array.isRequired,
        }),
      })
    ),
  }),
};

export default LangSwitcher;
