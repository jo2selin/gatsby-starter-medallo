import PropTypes from "prop-types";
import { GatsbyImage } from "gatsby-plugin-image";
import clsx from "clsx";
import { useTheme } from "@emotion/react";

interface HeroProps {
  data: {
    title: String;
    paragraphNode: { childMarkdownRemark: { html: string } };
    cover: { gatsbyImageData: any };
  };
}
const sectionHero = ({ data }: HeroProps) => {
  const theme = useTheme();

  return (
    <div className="hero-body">
      <div className="columns is-vcentered">
        <div className="column is-two-thirds">
          <h1 className={clsx("title is-2", theme.isDark && "has-text-white")}>
            {data.title}
          </h1>
          <div
            className={clsx("subtitle", theme.isDark && "has-text-white")}
            dangerouslySetInnerHTML={{
              __html: data.paragraphNode.childMarkdownRemark.html,
            }}
          />
        </div>
        <div className="column">
          <GatsbyImage image={data.cover.gatsbyImageData} alt={"alt"} />
        </div>
      </div>
    </div>
  );
};

sectionHero.propTypes = {
  data: PropTypes.object.isRequired,
};

export default sectionHero;
