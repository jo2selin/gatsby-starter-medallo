export function linkResolver(to: string, locale: string) {
  if (to === null) throw `link destination is null (${to}). locale: ${locale}`;

  const slug = to === "homepage" ? "" : to;

  const path = locale === "en" ? slug : `${locale}/${slug}`;
  let linkResolver = path;
  return linkResolver;
}

const breakpoints = [768, 1023, 1215, 1408];

export const mq = breakpoints.map((bp) => `@media (min-width: ${bp}px)`);
