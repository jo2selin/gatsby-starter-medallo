import React from "react";
import { graphql } from "gatsby";
import { HelmetDatoCms } from "gatsby-source-datocms";
import { ThemeProvider } from "@emotion/react";
import ThemeContext from "../context/ThemeContext";

import loadable from "@loadable/component";
import "bulma/css/bulma.css";
import "slick-carousel/slick/slick.css";
import "slick-carousel/slick/slick-theme.css";

const SectionHero = loadable(() => import("../components/sectionHero"));
const SectionHorizontalCard = loadable(() =>
  import("../components/sectionHorizontalCard")
);
const SectionVerticalCard = loadable(() =>
  import("../components/sectionVerticalCard")
);
const SidedContent = loadable(() => import("../components/sidedContent"));
const SectionSlideshow = loadable(() =>
  import("../components/sectionSlideshow")
);
const Section = loadable(() => import("../components/section"));
const Menu = loadable(() => import("../components/menu"));

const DisplaySection = ({ section }) => {
  switch (true) {
    case section.block.__typename === "DatoCmsSectionHero":
      return <SectionHero key={section.id} data={section.block} />;
    case section.block.__typename === "DatoCmsSectionHorizontalCard" &&
      section.block.layout === "slideshow":
      return <SectionSlideshow key={section.id} data={section.block} />;
    case section.block.__typename === "DatoCmsSectionHorizontalCard":
      return <SectionHorizontalCard key={section.id} data={section.block} />;
    case section.block.__typename === "DatoCmsSectionVerticalCard":
      return <SectionVerticalCard key={section.id} data={section.block} />;
    case section.block.__typename === "DatoCmsSectionSidedContent":
      return <SidedContent key={section.id} data={section.block} />;
    default:
      return null;
  }
};

const Page = ({ data, pageContext }) => {
  return (
    <ThemeContext.Consumer>
      {(theme) => {
        return (
          <main className={theme.dark ? "dark" : "light"}>
            <HelmetDatoCms seo={data.datoCmsPage.seoMetaTags}>
              <link id="icon" rel="icon" href="icon-specific-for-this-page" />
            </HelmetDatoCms>
            {data.datoCmsMenu && (
              <Menu
                data={data.datoCmsMenu}
                _allSlugLocales={pageContext._allSlugLocales}
                allWebsiteLocales={pageContext.allWebsiteLocales}
                locale={pageContext.locale}
              />
            )}
            {data.datoCmsPage.blocks?.map((section) => (
              <ThemeProvider key={section.id} theme={section.theme || {}}>
                <Section data={section}>
                  <DisplaySection section={section} />
                </Section>
              </ThemeProvider>
            ))}
          </main>
        );
      }}
    </ThemeContext.Consumer>
  );
};

export default Page;

export const query = graphql`
  query IndexQuery($locale: String, $slug: String) {
    datoCmsMenu {
      id
      locale
      items {
        __typename
        ... on DatoCmsLink {
          ...FragmentLink
        }
        ... on DatoCmsSubmenu {
          id
          title
          items {
            ...FragmentLink
          }
        }
        ... on DatoCmsButton {
          ...FragmentButton
        }
      }
    }
    datoCmsPage(locale: { eq: $locale }, slug: { eq: $slug }) {
      locale
      seoMetaTags {
        ...GatsbyDatoCmsSeoMetaTags
      }
      blocks {
        __typename
        ... on DatoCmsSection {
          id
          title
          paragraphNode {
            id
            childMarkdownRemark {
              html
            }
          }
          theme {
            ...FragmentTheme
          }
          size
          block {
            __typename
            ... on DatoCmsSectionHero {
              id
              title
              paragraphNode {
                id
                childMarkdownRemark {
                  html
                }
              }
              cover {
                gatsbyImageData
              }
              buttons {
                ...FragmentButton
              }
            }
            ... on DatoCmsSectionHorizontalCard {
              id
              layout
              cards {
                ...FragmentCards
              }
            }
            ... on DatoCmsSectionVerticalCard {
              name
              cards {
                ...FragmentCards
              }
            }
            ... on DatoCmsSectionSidedContent {
              id
              side
              blocks {
                ... on DatoCmsCard {
                  ...FragmentCards
                }
              }
            }
          }
        }
      }
    }
  }
`;
