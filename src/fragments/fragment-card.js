import { graphql } from "gatsby";
export const CardsFragment = graphql`
  fragment FragmentCards on DatoCmsCard {
    id
    title
    paragraphNode {
      childMarkdownRemark {
        id
        html
      }
    }
    icon
    image {
      title
      alt
      gatsbyImageData
    }
    buttons {
      ...FragmentButton
    }
    theme {
      ...FragmentTheme
    }
  }
`;
