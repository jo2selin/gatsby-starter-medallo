import { graphql } from "gatsby";
export const ThemeFragment = graphql`
  fragment FragmentTheme on DatoCmsTheme {
    isDark
    background {
      hex {
        rgb
        hex
      }
    }
    primary {
      hex {
        rgb
        hex
      }
    }
    secondary {
      hex {
        rgb
        hex
      }
    }
  }
`;
