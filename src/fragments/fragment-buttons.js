import { graphql } from "gatsby";
export const ButtonsFragment = graphql`
  fragment FragmentButton on DatoCmsButton {
    id
    outlined
    title
    _allTitleLocales {
      locale
      value
    }
    link {
      ...FragmentLink
    }
    theme {
      ...FragmentTheme
    }
  }
`;
