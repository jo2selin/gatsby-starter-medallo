import { graphql } from "gatsby";
export const LinkFragment = graphql`
  fragment FragmentLink on DatoCmsLink {
    id
    title
    _allTitleLocales {
      locale
      value
    }
    internalLink {
      name
      slug
      locale
      _allSlugLocales {
        locale
        value
      }
    }
    externalLink
    description
    _allDescriptionLocales {
      value
      locale
    }
  }
`;
